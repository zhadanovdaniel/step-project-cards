import { showAllCards } from "../showAllCards.js";

let token = '13f6d578-941e-420e-b6d5-eaeb1f133f6b';

export async function getAllCards() {

    try {
        const queryGET = await fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
        });

        const queryJSON = await queryGET.json();
		queryJSON.forEach(card => {
            showAllCards(card);
        });

    } catch (error) {
        console.error('Виникла проблема з fetch-запитом: ' + error);
    }
}

