
export function showAllCards(card) {
    // Знаходження контейнера, в якому потрібно відобразити картку
    const cardContainer = document.querySelector('.card__container');
    
    // Визначення типу лікаря з поля 'doctor' у картці
    const doctorType = card.doctor.toLowerCase(); // Перетворення на нижній регістр для порівняння
    
    // Створення HTML-коду для відображення картки відповідного лікаря
    let cardHTML = '';
    if (doctorType === 'стоматолог') {
        cardHTML = `<div class="card" style="width: 20vw">
            <p>ID: ${card.id}</p>
            <p>Title: Візит до стоматолога</p>
            <p>Doctor: Стоматолог</p>
            <p>Purpose: ${card.purpose}</p>
            <p>Description: ${card.description}</p>
            <p>Urgency: ${card.urgency}</p>
            <p>Initials: ${card.initials}</p>
            <p>Date: ${card.date}</p>
        </div>`;
    } else if (doctorType === 'кардіолог') {
        cardHTML = `<div class="card" style="width: 20vw">
            <p>ID: ${card.id}</p>
            <p>Title: Візит до кардіолога</p>
            <p>Doctor: Кардіолог</p>
            <p>Purpose: ${card.purpose}</p>
            <p>Description: ${card.description}</p>
            <p>Urgency: ${card.urgency}</p>
            <p>Initials: ${card.initials}</p>
            <p>Pressure: ${card.pressure}</p>
            <p>Імт: ${card.bmi}</p>
            <p>Diseases: ${card.diseases}</p>
            <p>Age: ${card.age}</p>
        </div>`;
    } else if (doctorType === 'терапевт') {
        cardHTML = `<div class="card" style="width: 20vw">
            <p>ID: ${card.id}</p>
            <p>Title: Візит до терапевта</p>
            <p>Doctor: Терапевт</p>
            <p>Purpose: ${card.purpose}</p>
            <p>Description: ${card.description}</p>
            <p>Urgency: ${card.urgency}</p>
            <p>Initials: ${card.initials}</p>
            <p>Age: ${card.age}</p>
        </div>`;
    }

    // Додавання картки до контейнера
    cardContainer.insertAdjacentHTML('beforeend', cardHTML);
}
